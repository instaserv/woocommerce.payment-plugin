<?php
/*
 * Plugin Name: QRtize
 * Plugin URI: https://wordpress.org/plugins/qrtize
 * Description: QRtize payment gateway
 * Author: Fedorenko  Dmytro / Fabian Gutierrez
 * Author URI: https://qrtize.app/
 * Version: 0.0.1
 * WC tested up to: 
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: qrtize
 */
/**
 * import options file
 */
include_once("includes/options.php");
include_once("includes/wc-qrtize-template-hooks.php");

if (!defined('ABSPATH')) {
    exit;
}
/**
 * Init Plugin after wordpress load the plugins
 */
add_action( 'plugins_loaded', 'qrtize_plugin_init' );

function qrtize_plugin_init(){
	/**
	 * Tell woocommerced to init payment gateway
	 */
	add_filter( 'woocommerce_payment_gateways', 'add_qrtize_payment_gateway' );
	function add_qrtize_payment_gateway( $gateways ) {
		$gateways[] = 'QRtize_payment_gateway';
		return $gateways;
	}

	/**
	 * Create payment gateway	
	 */
	if (!class_exists('QRTize_payment_gateway')) {
		include_once("includes/class-qrtize-payment-gateway.php");
		new  QRTize_payment_gateway();
	}

	/**
	 * Create Webhook Handler
	 */
	if (!class_exists('WC_QrTize_Webhook_Handler')){
		include_once("includes/class-wc-qrtize-webhook-handler.php");
		new  WC_QrTize_Webhook_Handler();
	}

	/**
	 * Add plugin sources
	 */
	add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'qrtize_plugin_action_links');
	
	function qrtize_plugin_action_links($links) {
		$setting_link = admin_url('admin.php?page=wc-settings&tab=checkout&section=qrtize_plugin'); //ToDO should change!!!!
		$plugin_links = array(
			'<a href="' . $setting_link . '">' . __('Settings', 'qrtize') . '</a>',         
		);
		return array_merge($plugin_links, $links);
	}
	
	/**
	 * Add styles to plugin (currently they are embed in the widget.js using webpack)
	 */
	// add_action('wp_enqueue_scripts','register_qrtize_plugin_styles');
	// function register_qrtize_plugin_styles(){
	// 	wp_enqueue_style( 'qrtize_plugin_style', plugins_url( 'assets/css/styles.css' , __FILE__ ) );
	// }	
}

function on_activation(){
	createDB();
}


function on_deactivation(){
	
}

function on_uninstall(){
	removeDB();
}

function createDB(){
	global $wpdb;
	$table_name = $wpdb->get_blog_prefix() . 'qrtize_plugin';
	$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset} COLLATE {$wpdb->collate}";
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	$sql = "CREATE TABLE {$table_name} (
		id int(11) unsigned NOT NULL auto_increment,
		display_type varchar(50) NOT NULL default '',
		merchant_api_key varchar(50),
		PRIMARY KEY  (id)
	) {$charset_collate};";
	dbDelta( $sql );

	if(!empty($wpdb->last_error)){
		consoleLog("MySQL error occurred when the table {$table_name} was created: {$wpdb->last_error}");
	}
}

function removeDB(){
	global $wpdb;
	$table_name = $wpdb->get_blog_prefix() . 'qrtize_plugin';
	$wpdb->query( "DROP TABLE IF EXISTS $table_name" );

	if(!empty($wpdb->last_error)){
		consoleLog("MySQL error occurred when the table {$table_name} was droped: {$wpdb->last_error}");
	}
}

function consoleLog($message){
	echo '<script>console.log("'.$message.'");</script>';
}


register_activation_hook(__FILE__, 'on_activation');
register_deactivation_hook(__FILE__, 'on_deactivation');
register_uninstall_hook(__FILE__, 'on_uninstall');
