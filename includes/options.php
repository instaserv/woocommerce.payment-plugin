<?php 



function qrtize_settings_page_output() {
    ?>

<div class="wrap">
		<h2><?php echo get_admin_page_title() ?></h2>

		<form action="options.php" method="POST">
			<?php
				settings_fields( 'qrtize_settings_page' );     // hidden protective fields
				do_settings_sections( 'qrtize_settings_page' ); //sections with settings (options). We only have one 'section_id'
				submit_button();
			?>
		</form>
	</div>

<?php

}

function register_qrtize_settings(){
    register_setting( 'qrtize_settings_page', 'display_type', 'sanitize_callback' );
    register_setting( 'qrtize_settings_page', 'merchant_api_key', 'sanitize_callback' );

	add_settings_section( 'section_1', '', '', 'qrtize_settings_page' ); 

	add_settings_field('field1', 'Set display type', 'set_display_type', 'qrtize_settings_page', 'section_1' );
	add_settings_field('field2', 'Set merchant API key', 'set_merchant_api_key', 'qrtize_settings_page', 'section_1' );
}

function set_display_type(){
	$val = get_option('display_type')
	?>
    <select name="display_type">
        <option value="inline" <?php selected($val, "inline"); ?>>Inline</option>
        <option value="popup" <?php selected($val, "popup"); ?>>Popup</option>
    </select>
	<?php
}

function set_merchant_api_key(){
	$val = get_option('merchant_api_key');
	?>
	<input type="text" name="merchant_api_key" value="<?php echo esc_attr( $val ) ?>" />
	<?php
}

## Data cleaning
function sanitize_callback( $options ){ 
	foreach( $options as $name => & $val ){
		if( $name == 'merchant_api_key' ) $val = strip_tags( $val );
		if( $name == 'display_type' ) $val = intval( $val );
    }

	return $options;
}

?>