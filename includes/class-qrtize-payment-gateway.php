<?php
class QRTize_payment_gateway extends WC_Payment_Gateway
{
	public function __construct()
	{
		if (!session_id()) {
			session_start();
		}

		$this->id = 'qrtize_plugin'; // payment gateway plugin ID
		$this->method_title = 'QRTize Gateway';
		$this->method_description = 'QrTize Payment gateway';

		// gateways can support subscriptions, refunds, saved payment methods,
		// for now, only simple payments
		$this->supports = array(
			'products'
		);

		// Method with all the options fields
		$this->init_form_fields();

		// Load the settings.
		$this->init_settings();
		$this->has_fields = false;
		$this->title = $this->get_option('title');
		$this->description = $this->get_option('description');
		$this->enabled = $this->get_option('enabled');
		$this->testmode = 'yes' === $this->get_option('testmode');
		$this->display_type = $this->get_option('display_type');
		$this->server_merchant_api_key = $this->testmode ? $this->get_option('test_server_merchant_api_key') : $this->get_option('live_server_merchant_api_key');
		$this->widget_merchant_api_key = $this->testmode ? $this->get_option('test_widget_merchant_api_key') : $this->get_option('live_widget_merchant_api_key');

		// This action hook saves the settings
		add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
		// This action listens the webhook from QrTize API
		add_action('woocommerce_api_complete_process_payment', array($this, 'webhook_complete_process_payment'));
	}

	/**
	 * Plugin options, we deal with it in Step 3 too
	 */
	public function init_form_fields()
	{
		$this->form_fields = array(
			'enabled' => array(
				'title'       => 'Enable/Disable',
				'label'       => 'Enable QRTize Payment Gateway',
				'type'        => 'checkbox',
				'description' => '',
				'default'     => 'no'
			),
			'title' => array(
				'title'       => __( 'Title', 'woocommerce'),
				'type'        => 'text',
				'description' => 'This controls the title which the user sees during checkout.',
				'default'     => 'QRTize (easy Qr code payment)',
				'desc_tip'    => true,
			),
			'description' => array(
				'title'       => 'Description',
				'type'        => 'textarea',
				'description' => 'This controls the description which the user sees during checkout.',
				'default'     => 'Pay with your credit card via our super-cool payment gateway.',
			),
			'testmode' => array(
				'title'       => 'Test mode',
				'label'       => 'Enable Test Mode',
				'type'        => 'checkbox',
				'description' => 'Place the payment gateway in test mode using test API keys.',
				'default'     => 'yes',
				'desc_tip'    => true,
			),
			'display_type' => array(
				'title'       => 'Display',
				'type' => 'select',
				'options' => array(
					'inline' => 'Inline',
					'popup' => 'Popup'
				),
				'default' => 'inline',
			),
			'webhook' => array(
				'title'=>'Webhook Url (set it on QrTize Merchant Console)',
				'type'=>'title',
				'disabled'=> true,
				'description'=> $this-> get_api_url_webhook_description()
			),
			'test_server_merchant_api_key' => array(
				'title'       => 'Test server merchant API key',
				'type'        => 'text'
			),
			'live_server_merchant_api_key' => array(
				'title'       => 'Live server merchant API key',
				'type'        => 'password'
			),
			'test_widget_merchant_api_key' => array(
				'title'       => 'Test widget merchant API key',
				'type'        => 'text'
			),
			'live_widget_merchant_api_key' => array(
				'title'       => 'Live widget merchant API key',
				'type'        => 'password'
			),
		);
	}

	/**
	 * You will need it if you want your custom credit card form, Step 4 is about it
	 */
	public function payment_fields()
	{
		// we need JavaScript to process a token only on cart/checkout pages, right?
		if (!is_cart() && !is_checkout() && !isset($_GET['pay_for_order'])) {
			echo '<script>console.log("not a good place for qrtize")</script>';
			return;
		}
		// if our payment gateway is disabled, we do not have to enqueue JS too
		if ('no' === $this->enabled) {
			echo '<script>console.log("qrtize disabled")</script>';
			return;
		}

		/**
		 * Disable Plugin for test mode
		 */
		if (!$this->testmode && !is_ssl()) {
			echo 'QRTize payment gateway PRODUCTION MODE is not available without SSL';
			return;
		}

		if (!$this->server_merchant_api_key){
			echo 'Missing QRtize server API key, go to the merchant console to generate a new key';
			return;
		}

		if (!$this->widget_merchant_api_key){
			echo 'Missing QRtize Widget API key, go to the merchant console to generate a new key';
			return;
		}

		$operationId = $this->getPublicOperationId();
		$this->loadPluginScript($operationId);
	}

	/*
		 * Custom CSS and JS, in most cases required only when you decided to go with a custom credit card form
		 */
	public function payment_scripts()
	{
	}

	/*
 		 * Fields validation, more in Step 5
		 */
	public function validate_fields()
	{
	}

	/*
	 * We're processing the payments here, everything about it is in Step 5
	 */
	public function process_payment($order_id)
	{
		global $woocommerce;
		$_SESSION["qrtize_popup_show"] = array(
			"order_id" => $order_id
		);

		// Redirect to popup QRCODE
		return array(
			'result' => 'success',
			'redirect' => wc_get_checkout_url() //ToDO make dinamic!
		);
	}

	public function webhook_complete_process_payment(){
		$response = wp_remote_post('{payment processor endpoint}', $args); //ToDO  запрос на сервер проверка оперейшинИд

		if (!is_wp_error($response)) {
			if ($body['response']['responseCode'] == 'APPROVED') {
				$order = wc_get_order($_GET['id']);
				$order->payment_complete();
				$order->reduce_order_stock();

				// some notes to customer (replace true with false to make it private)
				$order->add_order_note('Hey, your order is paid! Thank you!', true);

				update_option('webhook_debug', $_GET);

				// Empty cart
				$woocommerce->cart->empty_cart();
				return array(
					'result' => 'success',
					'redirect' => $this->get_return_url($order)
				);
			} else {
				wc_add_notice('Please try again.', 'error');
				return;
			}
		} else {
			wc_add_notice('Connection error.', 'error');
			return;
		}
	}

	private function getOrderInfo($order_id)
	{
		$order = wc_get_order($order_id);
	}

	private function prepareData()
	{
		// echo var_dump( WC()->cart->get_totals() );

		$cart_items = $this->prepare_products_data();
		$totals = WC()->cart->get_totals();
		$body = [
			"mode" => 'capture',
			"externalId" => "order_id_mock_" . time(),
			// "shippingDetails" => null,
			// "shippingAddress" => [
			// 	"addressLine1" => "",
			// 	"city" => "",
			// 	"state" => "",
			// 	"isoCountry" => "",
			// 	"postalCode" => "",
			// 	"title" => "",
			// 	"firstName" => "",
			// 	"lastName" => ""
			// ],
			"lines" => $cart_items,
			"taxes" => [],
			"currency" => "USD", //get_woocommerce_currency(),
			"totalDiscounts" => (int)($totals["discount_total"] * 100),
			"totalCouponsSavings" => 0, // doesn't exist in wc
			"totalShippingCost" => (int)($totals["shipping_total"] * 100),
			"totalTax" => (int)($totals["total_tax"] * 100),
			"totalPriceExcludingTax" => (int)(($totals["total"] - $totals["total_tax"]) * 100),
			"totalPrice" => (int)($totals["total"] * 100),
			"extra" => null
		];
		
		// echo json_encode($body);
		return $body;
	}

	private function prepare_products_data(){
		$cart_items = array();
		foreach (WC()->cart->get_cart() as $cart_item) {
			$price = $cart_item['data']->get_price();
			$lineTotalTax = 0;
			$lineTotalDiscount = 0;
			$lineTotalCouponsSavings = 0;
			$lineTotalPrice = ($price + $lineTotalTax - $lineTotalDiscount - $lineTotalCouponsSavings) * intval($cart_item['quantity']);
			$imageUrl = get_the_post_thumbnail_url($cart_item['product_id']) ? get_the_post_thumbnail_url($cart_item['product_id']) : NULL; 
			$attributes = $this->prepare_product_attributes_data($cart_item);
			
			$item = [
				"title" => $cart_item['data']->get_title(),
				"type" => 'sku',
				"refundable" => true,

				"details" => [
					"id" =>  strval($cart_item['product_id']),
					"unit" => 'quantity',
					"quantity" => intval($cart_item['quantity']),
					"price" => intval($price * 100),
					"totalPrice" => intval($lineTotalPrice * 100),
					"totalTax" => intval($lineTotalTax * 100),
					"totalDiscount" => intval($lineTotalDiscount * 100),
					"totalCouponsSavings" => intval($lineTotalCouponsSavings * 100),
					"attributes" => $attributes,
					"imageUrl" => $imageUrl,
				]
			];
			array_push($cart_items, $item);
		}
		return $cart_items;
	}

	/**
	 * Format attributes
	 */
	private function  prepare_product_attributes_data($cart_item){
		$result = array();
		if(!$cart_item["data"]->is_type('variation')){
			return $result;
		}
		$product = new WC_Product_Variation( $cart_item['variation_id'] );
		
		$variations = $product->get_variation_attributes(false);

		foreach($variations as $taxonomy => $value){
			$name = str_replace("attribute_", "", $taxonomy);

			$formated_variation =[
				"title" => $name, 
				"value" => $value,
				"type" => $this->get_product_attribute_type($taxonomy),
			];
			array_push($result, $formated_variation);
		}
		return $result;
	}

	/**
	 * Get attribute names
	 */
	private function get_product_attribute_type($attribute_name){
		$lowercase =  strtolower($attribute_name);

		if ($this->str_contains($lowercase, 'size')){
			return 'size';
		} else if($this->str_contains($lowercase, 'color')){
			return 'color';
		} else {
			return 'custom';
		}
	}

	private function str_contains($string, $contains){
		return strpos($string, $contains) !== false;
	}

	private function getPublicOperationId()
	{
		$body = $this->prepareData();

		// var_dump($this->server_merchant_api_key); 
		// var_dump($this->widget_merchant_api_key); 
		// var_dump("======");
		/**
		 * Send request to create the order
		 */
		$response = wp_remote_post(
			'https://dev-api.instaserv.app/retail/v1/orders',
			array(
				'method' => 'POST',
				'headers' => array(
					'Authorization' => "Bearer " . $this->server_merchant_api_key,
					'Content-Type' => 'application/json'
				),
				'body' => json_encode($body)
			)
		);
		if ($response['body']) {
			$json = json_decode($response['body']);
			return $json->operationPublicId;
		}
	}

	/**
	 * Adds the plugin script to the webpage
	 */
	private function loadPluginScript($operationId)
	{
		// load widget config
		// load widget code
		echo '<fieldset id="wc-' . esc_attr($this->id) . '-cc-form" class="wc-credit-card-form wc-payment-form" style="background:transparent;">';

		echo '
			<div id="div-id-to-put-qr-code"></div>
			<script>
				setTimeout(()=>{
				(function (w,d,s,o,f,js,fjs) {
					w["instaServSdk-Widget"]=o;w[o] = w[o] || function () { (w[o].q = w[o].q || []).push(arguments) };
					js = d.createElement(s), fjs = d.getElementsByTagName(s)[0];
					js.id = o; js.src = f; js.async = 1; fjs.parentNode.insertBefore(js, fjs);
				}(window, document, "script", "IS", "' . plugin_dir_url(dirname(__FILE__)) . 'assets/widget.js"));
		
				IS("init", { 
					type: "' . $this->display_type . '",
					el: "div-id-to-put-qr-code",
					requestFrequency: 1000,
					opId: "' . $operationId . '",
					size: 154,
					token: "' . $this->widget_merchant_api_key . '",
					wcGetOrderDetailsEndpoint: "'.get_site_url().WC_AJAX::get_endpoint('get_order_details').'",
				});
				IS("show-qr-code");
			}, 1000)
			</script>
			';
		echo '<div class="clear"></div></fieldset>';
	}

	public static function get_api_url(){
		return add_query_arg( 'wc-api', 'wc_qrtize', trailingslashit( get_home_url() ) );
	}

	private function get_api_url_webhook_description(){
		return sprintf( __( 'You must add the following webhook endpoint <strong style="background-color:#ddd;">&nbsp;%s&nbsp;</strong> to your <a href="https://merchant.qrtize.com/dashboard/webhooks" target="_blank">Qrtize account settings</a>. This will enable you to receive notifications on the charge statuses.', 'woocommerce-gateway-qrtize' ), QRTize_payment_gateway::get_api_url() );	
	}
}
