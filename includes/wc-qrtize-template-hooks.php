<?php
/**
 * QRTize WooCommerce Template Hooks
 *
 *
 * @package QRTize
 * @version 0.0.1
 */

defined( 'ABSPATH' ) || exit;

/**
 * Cart.
 *
 * @see qrtize_wc_add_btn_after_proceed_to_checkout()
 */
add_action( 'woocommerce_proceed_to_checkout', 'qrtize_wc_add_btn_after_proceed_to_checkout', 25 );
if ( ! function_exists( 'qrtize_wc_add_btn_after_proceed_to_checkout' ) ) {
    /**
	 * Add the Qrtize payment button on cart page.
	 */
	function qrtize_wc_add_btn_after_proceed_to_checkout() {
	?>
		<a href="javascript:showPayBox();" class="qrtize-button button">
			<?php esc_html_e( 'QRTize (easy QR code payment)', 'qrtize' ); ?>
		</a>
		<style>
			.qrtize-button {
				display:block;
				text-align:center;
				margin-top:10px;				
			}
            #qrtizeWidgetWrapper {
                display:none;
            }
		</style>
        <?php
            $obj = new QRTize_payment_gateway();
            $obj->payment_fields();
        ?>
        <script>
            function showPayBox(){
                document.getElementById('qrtizeWidgetWrapper').style.display = "flex";
            }
        </script>
	<?php 
	}
}
