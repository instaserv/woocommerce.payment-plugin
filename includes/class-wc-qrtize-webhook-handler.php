<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WC_QrTize_Webhook_Handler {
    public function __construct() {
		// $this->retry_interval = 2;
		// $stripe_settings      = get_option( 'woocommerce_stripe_settings', array() );
		// $this->testmode       = ( ! empty( $stripe_settings['testmode'] ) && 'yes' === $stripe_settings['testmode'] ) ? true : false;
		// $secret_key           = ( $this->testmode ? 'test_' : '' ) . 'webhook_secret';
		// $this->secret         = ! empty( $stripe_settings[ $secret_key ] ) ? $stripe_settings[ $secret_key ] : false;
		add_action( 'woocommerce_api_wc_qrtize', array( $this, 'check_for_webhook' ) );
	}

    public function check_for_webhook(){
        // return if webhook isn't from our API
        if ( ( 'POST' !== $_SERVER['REQUEST_METHOD'] )
			|| ! isset( $_GET['wc-api'] )
			|| ( 'wc_qrtize' !== $_GET['wc-api'] )
		) {
			return;
        }

        $request_body    = file_get_contents( 'php://input' );
		$request_headers = array_change_key_case( $this->get_request_headers(), CASE_UPPER );

        if ( $this->is_valid_request( $request_headers, $request_body ) ) {
			error_log('WC_QrTize_Webhook_Handler.check_for_webhook, VALID REQUEST');
			
			$this->process_webhook( json_decode($request_body, true) );
			status_header( 200 );
            exit;
        } else {
			error_log('WC_QrTize_Webhook_Handler.check_for_webhook, INVALID REQUEST');
            status_header( 403 );
            exit;
        }
    }

	/**
	 * Check if webhook request is a valid one
	 */
    private function is_valid_request($request_headers, $request_body ){
        // // if no body or headers, invalid
        // if ( null === $request_headers || null === $request_body ) {
		// 	error_log('no body, no headers');
		// 	return false;
		// }
		
        // if ( ! empty( $request_headers['user-agent'] ) && ! preg_match( '/Qrtize/', $request_headers['USER-AGENT'] ) ) {
		// 	error_log('no user agent or valid user agent');
		// 	return false;
		// }
		
		
        // // secret is required for us
        // if ( empty( $this->secret ) ) {
		// 	error_log('no secret');
        //     return false;
        // } 

		// // check if signature header
		// if (! empty( $request_headers['x-qrtize-signature'] )){
		// 	error_log('no x-qrtize-signature header');
		// 	return false;
		// }
		
		
        // // check if signature is a valid json
		// $json = $this->parse_json($request_headers['x-qrtize-signature']);
		// if (!$json || $json.v || $json.t){
		// 	error_log('no json');
		// 	return false;
		// }
		
		// // verify timestamp
		// $timestamp = $json->t;
		// $signature = $json->v;
		// if ( abs( $timestamp - time() ) > 5 * MINUTE_IN_SECONDS ) {
		// 	error_log('no valid timestamp');
		// 	return;
		// }

		// // generate expected signature
		// // Generate the expected signature.
		// $signed_payload     = $timestamp . '.' . $request_body;
		// $expected_signature = hash_hmac( 'sha256', $signed_payload, $this->secret );

		// // Check if the expected signature is present.
		// if ( $expected_signature!== $signature ) {
		// 	return false;
		// }

		return true;
    }
 
    /**
	 * Gets the incoming request headers. Some servers are not using
	 * Apache and "getallheaders()" will not work so we may need to
	 * build our own headers.
	 *
	 * taken from https://github.com/woocommerce/woocommerce-gateway-stripe/blob/c6ca5552ead2b43c463fbdeecdea39179546b6b1/includes/class-wc-stripe-webhook-handler.php
	 */
	private function get_request_headers() {
		if ( ! function_exists( 'getallheaders' ) ) {
			$headers = array();

			foreach ( $_SERVER as $name => $value ) {
				if ( 'HTTP_' === substr( $name, 0, 5 ) ) {
					$headers[ str_replace( ' ', '-', ucwords( strtolower( str_replace( '_', ' ', substr( $name, 5 ) ) ) ) ) ] = $value;
				}
			}

			return $headers;
		} else {
			return getallheaders();
		}
	}

	private function parse_json($string){
		try {
			return json_decode($string);
		} catch (Exception $e) {
			return null;
		}
	}

	private function process_webhook($event){
		switch ( $event['type'] ) {
			case 'ORDER_PAID':
				$this->process_webhook_order_paid($event);

		}
	}

	private function process_webhook_order_paid($event){
		error_log('------------process_webhook_order_paid');
		$order = wc_create_order(array(
			'address'=>array(

			),
			'status'=>'wc-processing',
			'order_id' => $event['orderId'],
		));
		 $order->set_created_via( 'qrtize' );
		//  $order->set_customer_id( $data['user_id'] );
		 $order->set_currency( get_woocommerce_currency() );
		 $order->set_prices_include_tax( 'yes' === get_option( 'woocommerce_prices_include_tax' ) );
		//  $order->set_customer_note( isset( $data['order_comments'] ) ? $data['order_comments'] : '' );
		 $order->set_payment_method( 'QRTize_payment_gateway' );
		

		 // Line items
		 foreach( $event['lines'] as $line_item ) {
			$details = $line_item['details'];
			$product = wc_get_product( intval($details['id']));
			$order->add_product( $product, $details['quantity'] );
		}
		$order->calculate_totals();

		
		$order_id = $order->save();
		WC()->cart->get_cart_from_session(); // Load users session
		WC()->cart->empty_cart(true);
		WC()->session->set('cart', array());
		wc_reduce_stock_levels($order_id);


		 return $order_id;
	}
}
